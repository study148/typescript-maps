import { Map } from './Map';
import { User } from './User';
import { Company } from './Company';

const map = new Map(document.getElementById('map'));

const user = new User();
const company = new Company();

map.addMarker({ title: user.name, label: 'U', position: user.location });
map.addMarker({ title: company.name, label: 'C', position: company.location });
