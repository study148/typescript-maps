/**
 * Google map wrapper to hide methods
 */



export class Map {
  private map: google.maps.Map;

  constructor(element: HTMLElement) {
    this.map = new google.maps.Map(element, {
      zoom: 1,
      center: {
        lat: 0,
        lng: 0
      }
    });
  }

  addMarker(options: google.maps.MarkerOptions): void {
    Object.apply(options, { map: this.map });
    new google.maps.Marker(options);
  }
}
