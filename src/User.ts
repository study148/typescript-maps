import { name, address } from 'faker';

export class User {
  name: string;
  location: {
    lat: number;
    lng: number;
  };

  constructor() {
    this.name = name.firstName();
    this.location = {
      lat: Number(address.latitude()),
      lng: Number(address.longitude())
    };
  }
}
